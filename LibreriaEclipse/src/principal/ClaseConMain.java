package principal;
import java.util.Scanner;
import Metodos.Metodos;
public class ClaseConMain {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int n=0;
		do{
			System.out.println("MENU \n 1. Sumar \n 2. Listar alumnos \n 3. Crear una piramide \n 4. Calcular numeros primos ");
			n=in.nextInt();
			switch(n){
			case 1:
				System.out.println("Introduce los valores que vas a sumar");
				int n1=in.nextInt();
				int n2=in.nextInt();
				int resultado=Metodos.sumar(n1, n2);
				System.out.println("La suma es " + resultado);
				break;
			case 2:
				Metodos.listar();
				break;
			case 3:
				Metodos.piramides();
				break;
			case 4:
				Metodos.primo();
				break;
			case 5:
				System.out.println("Fin del programa");
				break;
			default:
				System.out.println("Numero incorrecto");
			
			}
		}while(n!=5);

	}

}
