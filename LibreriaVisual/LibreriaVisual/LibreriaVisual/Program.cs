﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVis
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            do
            {
                Console.WriteLine("MENU \n 1. Sumar \n 2. Listar alumnos \n 3. Crear una piramide \n 4. Calcular numeros primos \n 5. Salir");
                n = int.Parse(Console.ReadLine());
                switch (n)
                {
                    case 1:
                        Console.WriteLine("Introduce los valores que vas a sumar");
                        int n1 = int.Parse(Console.ReadLine());
                        int n2 = int.Parse(Console.ReadLine());
                        int resultado = LibreriaVS.Metodos1.sumar(n1, n2);
                        Console.WriteLine("La suma es " + resultado);
                        break;
                    case 2:
                        LibreriaVS.Metodos1.listar();
                        break;
                    case 3:
                        LibreriaVS.Metodos1.piramides();
                        break;
                    case 4:
                        LibreriaVS.Metodos1.primo();
                        break;
                    case 5:
                        Console.WriteLine("Fin del programa");
                        break;
                    default:
                        Console.WriteLine("Numero incorrecto");
                        break;
                }
            } while (n != 5);
        }
    }
}
