package principal;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import java.awt.SystemColor;
import java.awt.Panel;
import java.awt.Canvas;
import java.awt.Toolkit;
import javax.swing.JSlider;
import java.awt.Color;
import java.awt.Scrollbar;
import java.awt.ScrollPane;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.JEditorPane;
import javax.swing.JProgressBar;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;

public class VentanaEclipse extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Public\\Pictures\\Sample Pictures\\DsUHgMgWsAA7VtP.jpg"));
		setBackground(SystemColor.activeCaption);
		setTitle("Registro de juegos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 670, 333);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 991, 26);
		contentPane.add(menuBar);
		
		JMenu mnCuenta = new JMenu("Cuenta");
		menuBar.add(mnCuenta);
		
		JMenuItem mntmCerrarSesin = new JMenuItem("Cerrar sesi\u00F3n");
		mnCuenta.add(mntmCerrarSesin);
		
		JMenuItem mntmConfiguracinDeLa = new JMenuItem("Configuraci\u00F3n de la cuenta");
		mnCuenta.add(mntmConfiguracinDeLa);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmContacto = new JMenuItem("Contacto");
		mnAyuda.add(mntmContacto);
		
		JMenuItem mntmManual = new JMenuItem("Manual");
		mnAyuda.add(mntmManual);
		
		JMenu mnConfiguracin = new JMenu("Configuraci\u00F3n");
		menuBar.add(mnConfiguracin);
		
		JMenuItem mntmAjustes = new JMenuItem("Ajustes");
		mnConfiguracin.add(mntmAjustes);
		
		JMenuItem mntmParmetros = new JMenuItem("Par\u00E1metros");
		mnConfiguracin.add(mntmParmetros);
		
		JProgressBar progressBar = new JProgressBar();
		menuBar.add(progressBar);
		progressBar.setForeground(Color.GREEN);
		progressBar.setValue(50);
		
		JLabel lblConsola = new JLabel("Consola:");
		lblConsola.setBounds(35, 51, 48, 26);
		contentPane.add(lblConsola);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"PlayStation 4", "Xbox ONE", "Nintendo Swtich"}));
		comboBox.setBounds(93, 54, 133, 20);
		contentPane.add(comboBox);
		
		JCheckBox checkBox = new JCheckBox("SI");
		checkBox.setForeground(Color.GREEN);
		checkBox.setBounds(144, 254, 58, 23);
		contentPane.add(checkBox);
		
		JCheckBox checkBox_1 = new JCheckBox("NO");
		checkBox_1.setForeground(Color.RED);
		checkBox_1.setBounds(204, 254, 70, 23);
		contentPane.add(checkBox_1);
		
		JLabel lblNivelDeTrabajo = new JLabel("Nivel del jugador");
		lblNivelDeTrabajo.setBounds(35, 88, 103, 27);
		contentPane.add(lblNivelDeTrabajo);
		
		JLabel lblguardarDatos = new JLabel("\u00BFGuardar datos?");
		lblguardarDatos.setBounds(35, 252, 103, 27);
		contentPane.add(lblguardarDatos);
		
		Scrollbar scrollbar = new Scrollbar();
		scrollbar.setBounds(633, 26, 21, 269);
		contentPane.add(scrollbar);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(145, 90, 69, 22);
		contentPane.add(spinner);
		
		JButton btnNewButton = new JButton("Enviar partida");
		btnNewButton.setBounds(287, 253, 133, 25);
		contentPane.add(btnNewButton);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setBounds(144, 134, 130, 57);
		contentPane.add(editorPane);
		
		JLabel lblAadeTusJuegos = new JLabel("A\u00F1ade tus juegos");
		lblAadeTusJuegos.setBounds(35, 134, 103, 27);
		contentPane.add(lblAadeTusJuegos);
		
		JSlider slider = new JSlider();
		slider.setBounds(144, 202, 130, 23);
		contentPane.add(slider);
		
		JLabel lblDificultad = new JLabel("Dificultad");
		lblDificultad.setBounds(35, 198, 103, 27);
		contentPane.add(lblDificultad);
		
		JTree tree = new JTree();
		tree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("Juegos") {
				{
					DefaultMutableTreeNode node_1;
					node_1 = new DefaultMutableTreeNode("Steam");
						node_1.add(new DefaultMutableTreeNode("Rocket League"));
						node_1.add(new DefaultMutableTreeNode("PUBG"));
						node_1.add(new DefaultMutableTreeNode("GTA V"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Uplay");
						node_1.add(new DefaultMutableTreeNode("Assassins Creed"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Origin");
						node_1.add(new DefaultMutableTreeNode("Apex Legends"));
						node_1.add(new DefaultMutableTreeNode("pizza"));
						node_1.add(new DefaultMutableTreeNode("ravioli"));
						node_1.add(new DefaultMutableTreeNode("bananas"));
					add(node_1);
				}
			}
		));
		tree.setBounds(287, 44, 283, 198);
		contentPane.add(tree);
	}
}
